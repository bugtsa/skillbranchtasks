package com.bugtsa.skillbranchtasks.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.components.AuthViewComponent;
import com.bugtsa.skillbranchtasks.di.components.DaggerAuthViewComponent;
import com.bugtsa.skillbranchtasks.di.modules.AuthViewModule;
import com.bugtsa.skillbranchtasks.mvp.presenters.AuthPresenter;
import com.bugtsa.skillbranchtasks.utils.ConstantManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthPanel extends LinearLayout {

    private static final String TAG = AuthPanel.class.getSimpleName();
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;
    private int mCustomState = 1;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.login_email_edit_text)
    EditText mEmailEditText;

    @BindView(R.id.login_password_edit_text)
    EditText mPasswordEditText;

    @BindView(R.id.login_button)
    Button mLoginButton;

    @BindView(R.id.show_catalog_button)
    Button mShowCatalogButton;

    @Inject
    AuthPresenter mAuthPresenter;

    private Context mContext;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);

        AuthViewComponent component = DaggerService.getComponent(AuthViewComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(AuthViewComponent.class, component);
        }
        component.inject(this);

        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        mEmailEditText.addTextChangedListener(new CustomWatcher(mEmailEditText));
        mPasswordEditText.addTextChangedListener(new CustomWatcher(mPasswordEditText));
        showViewFromState();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }

    public void setCustomState(int state) {
        mCustomState = state;
        showViewFromState();
    }

    private void showViewFromState() {
        if (mCustomState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        expand(mAuthCard, mShowCatalogButton);
        mLoginButton.setEnabled(false);
    }

    private void showIdleState() {
        collapse(mAuthCard, mShowCatalogButton);
        mLoginButton.setEnabled(true);
    }

    public static void expand(final View expandingView, final View showCatalogButton) {
        showCatalogButton.setVisibility(GONE);
        expandingView.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targetHeight = expandingView.getMeasuredHeight();

        expandingView.getLayoutParams().height = 1;
        expandingView.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                expandingView.getLayoutParams().height = interpolatedTime == 1
                        ? LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                expandingView.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (targetHeight / expandingView.getContext().getResources().getDisplayMetrics().density));
        expandingView.startAnimation(a);
    }

    public static void collapse(final View collapsingView, final View showCatalogButton) {
        final int initialHeight = collapsingView.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    collapsingView.setVisibility(View.GONE);
                    showCatalogButton.setVisibility(VISIBLE);
                } else {
                    collapsingView.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    collapsingView.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (initialHeight / collapsingView.getContext().getResources().getDisplayMetrics().density));
        collapsingView.startAnimation(a);
    }

    public String getUserEmail() {
        return String.valueOf(mEmailEditText.getText());
    }

    public void setUserEmail(String userEmail) {
        mEmailEditText.setText(userEmail);
    }

    public String getUserPassword() {
        return String.valueOf(mPasswordEditText.getText());
    }

    public void setUserPassword(String userPassword) {
        mPasswordEditText.setText(userPassword);
    }

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    static class SavedState extends BaseSavedState {

        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }

    public class CustomWatcher implements TextWatcher {

        private boolean mWasEdited = false;

        private EditText mEditText;

        public CustomWatcher(EditText editText) {
            mEditText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mWasEdited) {
                mWasEdited = false;
                return;
            }
            checkCorrectInput(mEditText);
            mWasEdited = true;
        }
    }

    private void checkCorrectInput(EditText editText) {
        int indexEditText;
        try {
            switch (editText.getId()) {
                case R.id.login_email_edit_text:
                    indexEditText = ConstantManager.EMAIL_EDIT_TEXT_KEY;
                    break;
                case R.id.login_password_edit_text:
                    indexEditText = ConstantManager.PASSWORD_EDIT_TEXT_KEY;
                    break;
                default:
                    indexEditText = ConstantManager.EMAIL_EDIT_TEXT_KEY;
            }
            mAuthPresenter.editTextChanged(indexEditText);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    //region ================= DI =================

    private AuthViewComponent createDaggerComponent() {
        return DaggerAuthViewComponent.builder()
                .authViewModule(new AuthViewModule())
                .build();
    }

    //endregion
}
