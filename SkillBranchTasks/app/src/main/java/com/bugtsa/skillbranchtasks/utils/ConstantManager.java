package com.bugtsa.skillbranchtasks.utils;

public interface ConstantManager {
    int EMAIL_EDIT_TEXT_KEY = 0;

    int PASSWORD_EDIT_TEXT_KEY = 1;

    int MINIMUM_PRODUCT_QUANTITY = 1;

    String TAG_AUTH_FRAGMENT = "TAG_AUTH_FRAGMENT";

    String USER_EMAIL_KEY = "USER_EMAIL_KEY";
    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
    String PRODUCT_PARCELABLE_KEY = "PRODUCT_PARCELABLE_KEY";
    String USER_AVATAR_KEY = "USER_AVATAR_KEY";
    String BASKET_COUNT = "BASKET_COUNT";
}
