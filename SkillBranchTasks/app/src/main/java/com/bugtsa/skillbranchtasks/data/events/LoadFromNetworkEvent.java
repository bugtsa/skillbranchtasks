package com.bugtsa.skillbranchtasks.data.events;

public class LoadFromNetworkEvent {

    private boolean mIsLoad;

    public LoadFromNetworkEvent(boolean isLoad) {
        mIsLoad = isLoad;
    }

    public boolean isLoad() {
        return mIsLoad;
    }
}
