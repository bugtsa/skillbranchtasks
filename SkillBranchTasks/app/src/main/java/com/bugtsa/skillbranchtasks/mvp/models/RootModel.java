package com.bugtsa.skillbranchtasks.mvp.models;


import android.net.Uri;

public class RootModel extends AbstractModel {

    public Uri getSelectedImage() {
        return mDataManager.getPreferencesManager().loadUserAvatar();
    }

    public void saveCountProductInBucket(int countProductInBucket) {
        mDataManager.getPreferencesManager().saveBasketCounter(countProductInBucket);
    }

    public int getCountProductInBucket() {
        return mDataManager.getPreferencesManager().getBasketCounter();
    }
}
