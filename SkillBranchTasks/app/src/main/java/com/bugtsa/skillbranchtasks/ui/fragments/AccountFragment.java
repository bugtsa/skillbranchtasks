package com.bugtsa.skillbranchtasks.ui.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.databinding.FragmentAccountBinding;

public class AccountFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 40;
    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;

    FragmentAccountBinding mAccountBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        mAccountBinding = DataBindingUtil.bind(view);

        setupAppBar();

        return view;
    }

    private void setupAppBar() {
        mAccountBinding.appbarAccount.addOnOffsetChangedListener(this);
        mMaxScrollSize = mAccountBinding.appbarAccount.getTotalScrollRange();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;
            mAccountBinding.userAvatar.animate().scaleY(0).scaleX(0).setDuration(200).start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            mAccountBinding.userAvatar.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }
}
