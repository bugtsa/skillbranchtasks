package com.bugtsa.skillbranchtasks.utils;

public interface AppConfig {
    String BASE_URL = "http://anyUrl.ru";
    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;
}
