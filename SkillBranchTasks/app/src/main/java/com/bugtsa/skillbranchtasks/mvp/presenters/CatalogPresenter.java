package com.bugtsa.skillbranchtasks.mvp.presenters;

import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.scopes.CatalogScope;
import com.bugtsa.skillbranchtasks.mvp.models.AuthModel;
import com.bugtsa.skillbranchtasks.mvp.models.CatalogModel;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.ICatalogPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.ICatalogView;
import com.bugtsa.skillbranchtasks.mvp.views.IRootView;
import com.bugtsa.skillbranchtasks.ui.fragments.RootFragment;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mCatalogModel;

    private AuthModel mAuthModel;

    private List<ProductDto> mProductList;

    public CatalogPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        mAuthModel = new AuthModel();
    }

    private IRootView getRootView() {
        return mRootPresenter.getView();
    }

    //region ================= AbstractPresenter =================

    @Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mCatalogModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }
    }

    //endregion

    //region ================= ICatalogPresenter =================

    @Override
    public void clickOnBuyButton(int position) {
        if (getRootView() != null) {
            if (checkUserAuth()) {
                ProductDto product = mProductList.get(position);
                getRootView().showPutProductInBucketMessage(product.getProductName());
                getRootView().increaseProductCounter(product.getCount());
            } else {
                getView().showAuthScreen();
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    @Override
    public void saveAuthUser() {
        if (getView() != null) {
            if (!checkUserAuth()) {
                mAuthModel.saveUserData(getView().getAuthEmail(), getView().getAuthPassword());
                getView().hideAuthDialog();
            }
        }
    }

    @Override
    public void updateCountProductInBucket() {
        if (getRootView() != null) {
            getRootView().updateProductCounter(mCatalogModel.getCountProductInBucket());
        }
    }

    //endregion

    //region ================= DI =================

    private Component createDaggerComponent () {
        return DaggerCatalogPresenter_Component.builder()
                .component(DaggerService.getComponent(RootFragment.Component.class))
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootFragment.Component.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter presenter);
    }

    //endregion
}
