package com.bugtsa.skillbranchtasks.ui.fragments;


import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bugtsa.skillbranchtasks.BuildConfig;
import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.databinding.FragmentAuthBinding;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.components.AuthViewComponent;
import com.bugtsa.skillbranchtasks.di.components.DaggerAuthViewComponent;
import com.bugtsa.skillbranchtasks.di.modules.AuthViewModule;
import com.bugtsa.skillbranchtasks.di.scopes.AuthScope;
import com.bugtsa.skillbranchtasks.mvp.presenters.AuthPresenter;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.IAuthPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IAuthView;
import com.bugtsa.skillbranchtasks.ui.custom_views.AuthPanel;
import com.bugtsa.skillbranchtasks.utils.FragmentUtils;

import javax.inject.Inject;

public class AuthFragment extends Fragment implements IAuthView, View.OnClickListener{

    FragmentAuthBinding mBinding;

    @Inject
    AuthPresenter mAuthPresenter;

    AuthViewComponent component;

    private Animation animScale;
    private Animation animRotateToSun;
    private Animation animRotateAgainstSun;

    public static AuthFragment newInstance() {
        return new AuthFragment();
    }

    //region ================= Life cycle =================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        mBinding = DataBindingUtil.bind(view);

        component = DaggerService.getComponent(AuthViewComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(AuthViewComponent.class, component);
        }
        component.inject(this);

        mAuthPresenter.takeView(this);
        mAuthPresenter.initView();

        setupAnimation();
        setupButtonsListener();
        setupTypeFace();

        return view;
    }

    @Override
    public void onDestroyView() {
        mAuthPresenter.dropView();
        if (isRemoving()) {
            mAuthPresenter = null;
            component = null;
            DaggerService.unregisterScope(AuthScope.class);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        mAuthPresenter.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mAuthPresenter.onPause();
        super.onPause();
    }

    //endregion

    //region ================= Setup UI =================

    private void setupAnimation() {
        animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
        animRotateToSun = AnimationUtils.loadAnimation(getContext(), R.anim.anim_rotate_to_sun);
        animRotateAgainstSun = AnimationUtils.loadAnimation(getContext(), R.anim.anim_rotate_against_sun);
    }

    private void setupButtonsListener() {
        mBinding.loginButton.setOnClickListener(this);
        mBinding.loginButton.setEnabled(true);
        mBinding.showCatalogButton.setOnClickListener(this);
        mBinding.vkButton.setOnClickListener(this);
        mBinding.twButton.setOnClickListener(this);
        mBinding.fbButton.setOnClickListener(this);
    }

    private void setupTypeFace() {
        Typeface myFontBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/PTBebasNeueRegular.ttf");
        mBinding.appNameTextView.setTypeface(myFontBold);
    }

    //endregion

    //region ================= View =================

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                mAuthPresenter.clickOnLogin();
                break;
            case R.id.show_catalog_button:
                mAuthPresenter.clickOnShowCatalog();
                break;
            case R.id.vk_button:
                view.startAnimation(animRotateToSun);
                break;
            case R.id.tw_button:
                view.startAnimation(animRotateAgainstSun);
                break;
            case R.id.fb_button:
                view.startAnimation(animScale);
                break;
        }
    }

    //endregion

    //region ================= IAuthView =================

    @Override
    public void showErrorEmailEditText() {
        mBinding.loginEmailWrapper.setErrorEnabled(true);
        mBinding.loginEmailWrapper.setError(getString(R.string.email_error_message));
    }

    @Override
    public void showErrorPasswordEditText() {
        mBinding.loginPasswordWrapper.setErrorEnabled(true);
        mBinding.loginPasswordWrapper.setError(getString(R.string.password_error_message));
    }

    @Override
    public void showValidEmailEditText() {
        mBinding.loginEmailWrapper.setErrorEnabled(false);
    }

    @Override
    public void showValidPasswordEditText() {
        mBinding.loginPasswordWrapper.setErrorEnabled(false);
    }

    public IAuthPresenter getAuthPresenter() {
        return mAuthPresenter;
    }

    @Override
    public void enabledLoginButton() {
        mBinding.loginButton.setEnabled(true);
    }

    @Override
    public void disabledLoginButton() {
        mBinding.loginButton.setEnabled(false);
    }

    @Override
    public void setTextLoginButton(boolean isUserAuth) {
        if (isUserAuth) {
            mBinding.loginButton.setText(getString(R.string.exit));
        } else {
            mBinding.loginButton.setText(getString(R.string.enter));
        }
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mBinding.authPanel;
    }

    @Override
    public void showCatalogScreen() {
        FragmentUtils.replaceFragment(getActivity(), R.id.fragment_auth_container, new RootFragment(), false);
    }

    //endregion

    //region ================= IView =================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mBinding.coordinatorAuthView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrors(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.toString());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.not_correct_working_app));
        }
    }

    @Override
    public void showLoad() {
//        if (mProgressDialog == null) {
//            mProgressDialog = ProgressDialog.show(this, null, null);
//            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            mProgressDialog.setContentView(new ProgressBar(this));
//        }
//        mProgressDialog.show();
//        EventBus.getDefault().post(new StartLoadInViewEvent(6000));
    }

    @Override
    public void hideLoad() {
//        if (mProgressDialog != null) {
//            if (mProgressDialog.isShowing()) {
//                showMessage(getString(R.string.load_done));
//                mProgressDialog.hide();
//            }
//        }
    }

    //endregion

    //region ================= DI =================

    private AuthViewComponent createDaggerComponent() {
        return DaggerAuthViewComponent.builder()
                .authViewModule(new AuthViewModule())
                .build();
    }

    //endregion
}
