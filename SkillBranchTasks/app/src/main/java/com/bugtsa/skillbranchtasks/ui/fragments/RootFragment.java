package com.bugtsa.skillbranchtasks.ui.fragments;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bugtsa.skillbranchtasks.BuildConfig;
import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.SkillBranchTasksApplication;
import com.bugtsa.skillbranchtasks.data.events.StartLoadInViewEvent;
import com.bugtsa.skillbranchtasks.databinding.DrawerViewBinding;
import com.bugtsa.skillbranchtasks.databinding.FragmentRootBinding;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.components.DaggerPicassoComponent;
import com.bugtsa.skillbranchtasks.di.components.PicassoComponent;
import com.bugtsa.skillbranchtasks.di.modules.PicassoCacheModule;
import com.bugtsa.skillbranchtasks.di.scopes.RootScope;
import com.bugtsa.skillbranchtasks.mvp.presenters.RootPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IRootView;
import com.bugtsa.skillbranchtasks.utils.FragmentUtils;
import com.bugtsa.skillbranchtasks.utils.RoundedAvatarDrawable;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import dagger.Provides;

public class RootFragment extends Fragment implements IRootView, NavigationView.OnNavigationItemSelectedListener {

    FragmentRootBinding mRootBinding;

    DrawerViewBinding mDrawerHeaderBinding;

    protected ProgressDialog mProgressDialog;

    private int mCountProductInBucket = 0;

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    Picasso mPicasso;

    Component component;

    ActionBar mActionBar;

    private MaterialDialog mExitAppDialog;

    //region ================= Fragment =================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_root, container, false);
        mRootBinding = DataBindingUtil.bind(view);
        setHasOptionsMenu(true);

        component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        setupToolBar();
        setupDrawer();
        setupDrawerHeader();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();

        MenuItem item = mRootBinding.navigationView.getMenu().findItem(R.id.catalog_menu);
        item.setChecked(true);
        mRootBinding.navigationView.getMenu().performIdentifierAction(R.id.catalog_menu, 1);
        setupMaterialDialog();

        return view;
    }

    @Override
    public void onDestroyView() {
        mRootPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        String titleActionBar = "";
        int visibilityIconToolBar = 0;
        switch (item.getItemId()) {
            case R.id.account_menu:
                fragment = new AccountFragment();
                titleActionBar = getString(R.string.menu_my_account);
                visibilityIconToolBar = View.INVISIBLE;
                break;
            case R.id.catalog_menu:
                fragment = new CatalogFragment();
                visibilityIconToolBar = View.VISIBLE;
                break;
            case R.id.favorite_menu:
                fragment = new FavoriteFragment();
                titleActionBar = getString(R.string.menu_favorite);
                visibilityIconToolBar = View.INVISIBLE;
                break;
            case R.id.orders_menu:
                fragment = new OrdersFragment();
                titleActionBar = getString(R.string.menu_orders);
                visibilityIconToolBar = View.INVISIBLE;
                break;
            case R.id.notification_menu:
                fragment = new NotificationsFragment();
                titleActionBar = getString(R.string.menu_notification);
                visibilityIconToolBar = View.INVISIBLE;
                break;
        }

        if (fragment != null) {
            FragmentUtils.replaceFragment(getActivity(), R.id.fragment_root_container, fragment, true);
        }

        mRootBinding.drawerLayout.closeDrawer(GravityCompat.START);
        setActionBarTitle(titleActionBar);
        setVisibilityIconToolBar(visibilityIconToolBar);
        return true;
    }

    private void setVisibilityIconToolBar(int visibilityIconToolBar) {
        mRootBinding.fireBaseIcon.setVisibility(visibilityIconToolBar);
        mRootBinding.counterPanel.setVisibility(visibilityIconToolBar);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCountProductInBucket = mRootPresenter.getCountProductInBucket();
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (mRootBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mRootBinding.drawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        mExitAppDialog.show();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mRootPresenter.saveCountProductInBucket(mCountProductInBucket);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mRootBinding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    //endregion

    //region ================= IRootView =================

    @Override
    public void showPutProductInBucketMessage(String productName) {
        Snackbar.make(mRootBinding.coordinatorLayout,
                getString(R.string.message_product_buy_button) + productName + getString(R.string.message_success_buy_button),
                Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mRootBinding.coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrors(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.toString());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.not_correct_working_app));
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getActivity(), null, null);
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.setContentView(new ProgressBar(getActivity()));
        }
        mProgressDialog.show();
        EventBus.getDefault().post(new StartLoadInViewEvent(6000));
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                showMessage(getString(R.string.load_done));
                mProgressDialog.hide();
            }
        }
    }

    @Override
    public void increaseProductCounter(int increaseProduct) {
        mCountProductInBucket += increaseProduct;
        mRootBinding.count.setText(String.valueOf(mCountProductInBucket));
    }

    @Override
    public void updateProductCounter(int countProductInBucket) {
        mCountProductInBucket = countProductInBucket;
        mRootBinding.count.setText(String.valueOf(countProductInBucket));
    }

    //endregion

    //region ================= Setup UI =================

    /**
     * инициализирует NavigationDrawer
     */
    private void setupDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), mRootBinding.drawerLayout, mRootBinding.toolbar, R.string.open_drawer, R.string.close_drawer);
        mRootBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mRootBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupDrawerHeader() {
        mDrawerHeaderBinding = DataBindingUtil.bind(mRootBinding.navigationView.getHeaderView(0));
        insertDrawerAvatar(mRootPresenter.getUriDrawerAvatar());
    }

    /**
     * Устанавливает аватар пользователя со скруглёнными краями в Navigation Drawer
     *
     * @param selectedImage
     */
    private void insertDrawerAvatar(Uri selectedImage) {
        mPicasso.with(getActivity())
                .load(selectedImage)
                .fit()
                .centerCrop()
                .transform(new RoundedAvatarDrawable())
                .placeholder(R.drawable.user_avatar)
                .into(mDrawerHeaderBinding.drawerUserAvatarIv);
    }

    private void setupToolBar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mRootBinding.toolbar);

        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (mActionBar != null) {
            mActionBar.setTitle(R.string.my_name);
            mActionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setActionBarTitle(String titleActionBar) {
        mActionBar.setTitle(titleActionBar);
    }

    private void setupMaterialDialog() {
        mExitAppDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.confirm_exit)
                .content(R.string.content_exit)
                .positiveText(R.string.agree)
                .positiveColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                .negativeText(R.string.disagree)
                .negativeColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getActivity().moveTaskToBack(true);
                        getActivity().finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .build();
    }

    //endregion

    //region ================= DI =================

    private Component createDaggerComponent() {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(SkillBranchTasksApplication.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }
        return DaggerRootFragment_Component.builder()
                .picassoComponent(picassoComponent)
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootPresenter provideRootPresenter() {
            return new RootPresenter();
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @RootScope
    public interface Component {
        void inject(RootFragment fragment);

        RootPresenter getRootPresenter();
    }

    //endregion
}
