package com.bugtsa.skillbranchtasks.mvp.presenters;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.scopes.ProductScope;
import com.bugtsa.skillbranchtasks.mvp.models.ProductModel;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.IProductPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IProductView;

import javax.inject.Inject;

import dagger.Provides;

import static com.bugtsa.skillbranchtasks.utils.ConstantManager.MINIMUM_PRODUCT_QUANTITY;

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {

    private static final String TAG = ProductPresenter.class.getSimpleName();

    @Inject
    ProductModel mProductModel;

    private ProductDto mProductDto;

    private static ProductPresenter sProductPresenter = null;

    public static ProductPresenter newInstance() {

        if (sProductPresenter == null) {
            sProductPresenter = new ProductPresenter();
        }
        return sProductPresenter;
    }
    

    public ProductPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ProductPresenter.Component.class, component);
        }
        component.inject(this);
    }

    public void setProduct(ProductDto product) {
        mProductDto = product;
    }

    //region ================= AbstractPresenter =================

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProductDto);
        }
    }

    //endregion

    //region ================= IProductPresenter =================

    @Override
    public ProductDto addProduct() {
        mProductDto.addProduct();
        mProductModel.updateProduct(mProductDto);
        return mProductDto;
    }

    @Override
    public ProductDto deleteProduct() {
        if (mProductDto.getCount() > MINIMUM_PRODUCT_QUANTITY) {
            mProductDto.deleteProduct();
            mProductModel.updateProduct(mProductDto);
        }
        return mProductDto;
    }

    //endregion

    //region ============ DI ============
    
    private Component createDaggerComponent() {
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductModel provideProductModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = ProductPresenter.Module.class)
    @ProductScope
    interface Component {

        void inject(ProductPresenter presenter);
    }

    //endregion
}
