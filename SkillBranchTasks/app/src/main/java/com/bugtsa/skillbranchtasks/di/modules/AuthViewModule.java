package com.bugtsa.skillbranchtasks.di.modules;

import com.bugtsa.skillbranchtasks.di.scopes.AuthScope;
import com.bugtsa.skillbranchtasks.mvp.presenters.AuthPresenter;

import dagger.Provides;

@dagger.Module
public class AuthViewModule {

    @Provides
    @AuthScope
    AuthPresenter provideAuthPresenter() {
        return new AuthPresenter();
    }
}
