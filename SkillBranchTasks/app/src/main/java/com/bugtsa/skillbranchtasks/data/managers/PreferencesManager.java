package com.bugtsa.skillbranchtasks.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.bugtsa.skillbranchtasks.utils.ConstantManager;

import static com.bugtsa.skillbranchtasks.utils.ConstantManager.BASKET_COUNT;

public class PreferencesManager {

    private SharedPreferences mSharedPreferences;

    private Context mContext;

    /**
     * Присваивает SharedPreferences в переменную класса
     */
    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mContext = context;
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    public void removeAuthToken() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(ConstantManager.AUTH_TOKEN_KEY)
                .apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN_KEY, "");
    }

    public void saveUserEmail(String userEmail) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_EMAIL_KEY, userEmail);
        editor.apply();
    }

    public void removeUserEmail() {
        mSharedPreferences
                .edit()
                .remove(ConstantManager.USER_EMAIL_KEY)
                .apply();
    }

    public String getUserEmail() {
        return mSharedPreferences.getString(ConstantManager.USER_EMAIL_KEY, "");
    }

    public void saveUserAvatar(Uri uri) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_AVATAR_KEY, uri.toString());
        editor.apply();
    }

    public Uri loadUserAvatar() {
        return Uri.parse(mSharedPreferences.getString(ConstantManager.USER_AVATAR_KEY,
                "android.resource://com.bugtsa.skillbranchtasks/drawable/user_avatar"));
    }

    public void saveBasketCounter(int count) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(BASKET_COUNT, count);
        editor.apply();
    }

    public int getBasketCounter() {
        return mSharedPreferences.getInt(BASKET_COUNT, 0);
    }
}
