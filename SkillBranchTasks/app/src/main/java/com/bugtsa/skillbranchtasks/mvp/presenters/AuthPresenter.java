package com.bugtsa.skillbranchtasks.mvp.presenters;

import android.text.TextUtils;

import com.bugtsa.skillbranchtasks.data.events.LoadFromNetworkEvent;
import com.bugtsa.skillbranchtasks.data.events.StartLoadInViewEvent;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.scopes.AuthScope;
import com.bugtsa.skillbranchtasks.mvp.models.AuthModel;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.IAuthPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IAuthView;
import com.bugtsa.skillbranchtasks.ui.custom_views.AuthPanel;
import com.bugtsa.skillbranchtasks.utils.ConstantManager;
import com.bugtsa.skillbranchtasks.utils.LogUtils;
import com.bugtsa.skillbranchtasks.utils.ValidateUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import dagger.Provides;

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {

    private static final String TAG = AuthPresenter.class.getSimpleName();

    @Inject
    public AuthModel mAuthModel;

    private boolean isEmailValid;
    private boolean isPasswordValid;

    public AuthPresenter() {
        mAuthModel = new AuthModel();

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        LogUtils.d(TAG, "AuthPresenter: inject complete" );
    }

    //region ================= AbstractPresenter =================

    @Override
    public void initView() {
        if (getView() != null) {
            boolean isUserAuth = isUserAuth();
            if (isUserAuth) {
                getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
            } else {
                getView().enabledLoginButton();
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            }
//            getView().getAuthPanel().setAuthPresenter();
            getView().setTextLoginButton(isUserAuth);
        }
    }

    //endregion

    //region ================= IAuthPresenter =================

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (isCorrectInput()) {
                if (getView().getAuthPanel().isIdle()) {
                    mAuthModel.clearUserData();
                    getView().getAuthPanel().setUserEmail("");
                    getView().getAuthPanel().setUserPassword("");
                    getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
                } else {
                    mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                    getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                }
            } else {
                if (getView().getAuthPanel().isIdle()) {
                    mAuthModel.clearUserData();
                    getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
                    getView().disabledLoginButton();
                } else {
                    getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                }
            }
            getView().setTextLoginButton(isUserAuth());
        }
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        if (getView() != null) {
            getView().setTextLoginButton(isUserAuth());
        }
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            mAuthModel.loadDataFromNetwork();
            getView().showMessage("Показать каталог");

            getView().showCatalogScreen();
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public boolean isUserAuth() {
        return mAuthModel.isAuthUser();
    }

    @Override
    public boolean isCorrectInput() {
        if (getView() != null) {
            if (TextUtils.isEmpty(getView().getAuthPanel().getUserPassword()) &&
                    TextUtils.isEmpty(getView().getAuthPanel().getUserEmail())) {
                getView().enabledLoginButton();
                return false;
            } else {
                if (isEmailValid && isPasswordValid) {
                    getView().enabledLoginButton();
                    return true;
                } else {
                    getView().disabledLoginButton();
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    @Override
    public void editTextChanged(int editTextKey) {
        if (getView() != null) {
            if (editTextKey == ConstantManager.EMAIL_EDIT_TEXT_KEY) {
                isEmailValid = ValidateUtils.isValidEmail(getView().getAuthPanel().getUserEmail());
                if (isEmailValid) {
                    getView().showValidEmailEditText();
                } else {
                    if (getView().getAuthPanel().getUserEmail().equals("")) {
                        getView().showValidEmailEditText();
                    } else {
                        getView().showErrorEmailEditText();
                    }
                }
            } else if (editTextKey == ConstantManager.PASSWORD_EDIT_TEXT_KEY) {
                isPasswordValid = ValidateUtils.isValidPassword(getView().getAuthPanel().getUserPassword());
                if (isPasswordValid) {
                    getView().showValidPasswordEditText();
                } else {
                    if (getView().getAuthPanel().getUserPassword().equals("")) {
                        getView().showValidPasswordEditText();
                    } else {
                        getView().showErrorPasswordEditText();
                    }
                }
            }
            isCorrectInput();
        }
    }

    //endregion

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getLoadFromNetworkEvent(LoadFromNetworkEvent loadFromNetworkEvent) {
        if (loadFromNetworkEvent.isLoad()) {
            if (getView() != null) {
                getView().showLoad();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void getStartLoadInViewEvent(StartLoadInViewEvent startLoadInViewEvent) {
        try {
            if (getView() != null) {
                Thread.sleep(startLoadInViewEvent.getTimeLoop());
                getView().hideLoad();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //region ============ DI ============

    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthPresenter presenter);
    }

    private Component createDaggerComponent() {
        return DaggerAuthPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    //endregion
}
