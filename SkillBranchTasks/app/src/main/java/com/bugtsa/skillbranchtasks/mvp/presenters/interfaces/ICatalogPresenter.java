package com.bugtsa.skillbranchtasks.mvp.presenters.interfaces;


public interface ICatalogPresenter {

    void clickOnBuyButton(int position);

    boolean checkUserAuth();

    void saveAuthUser();

    void updateCountProductInBucket();
}
