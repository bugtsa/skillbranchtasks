package com.bugtsa.skillbranchtasks.di.components;

import com.bugtsa.skillbranchtasks.di.modules.ModelModule;
import com.bugtsa.skillbranchtasks.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {

    void inject(AbstractModel abstractModel);
}
