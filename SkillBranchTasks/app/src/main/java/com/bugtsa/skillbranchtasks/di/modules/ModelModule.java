package com.bugtsa.skillbranchtasks.di.modules;

import com.bugtsa.skillbranchtasks.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
