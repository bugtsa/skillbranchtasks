package com.bugtsa.skillbranchtasks.mvp.models;

import com.bugtsa.skillbranchtasks.data.events.LoadFromNetworkEvent;

import org.greenrobot.eventbus.EventBus;

public class AuthModel extends AbstractModel implements IAuthModel {

    public AuthModel() {
    }

    public boolean isAuthUser() {
        return !mDataManager.getPreferencesManager().getUserEmail().equals("") &&
                !mDataManager.getPreferencesManager().getAuthToken().equals("");
    }

    public void loginUser(String email, String password) {
        saveUserData(email, password);
        getDataFromNetwork();
    }

    public void loadDataFromNetwork() {
        getDataFromNetwork();
    }

    //region ================= IAuthModel =================

    @Override
    public void clearUserData() {
        mDataManager.getPreferencesManager().removeAuthToken();
        mDataManager.getPreferencesManager().removeUserEmail();
    }

    @Override
    public void saveUserData(String email, String password) {
        mDataManager.getPreferencesManager().saveUserEmail(email);
        mDataManager.getPreferencesManager().saveAuthToken(email + password);
    }

    @Override
    public void getDataFromNetwork() {
        EventBus.getDefault().post(new LoadFromNetworkEvent(true));
    }

    //endregion
}
