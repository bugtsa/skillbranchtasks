package com.bugtsa.skillbranchtasks.ui.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;
import com.bugtsa.skillbranchtasks.databinding.CardViewAuthPanelBinding;
import com.bugtsa.skillbranchtasks.databinding.FragmentCatalogBinding;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.scopes.CatalogScope;
import com.bugtsa.skillbranchtasks.mvp.presenters.CatalogPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.ICatalogView;
import com.bugtsa.skillbranchtasks.ui.fragments.adapters.CatalogAdapter;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    private static final String TAG = CatalogFragment.class.getSimpleName();

    FragmentCatalogBinding mBinding;

    CardViewAuthPanelBinding mBindingAuthDialog;

    @Inject
    CatalogPresenter mCatalogPresenter;

    Component component;

    private MaterialDialog mAuthDialog;

    public CatalogFragment() {

    }

    //region ================= Life cycle =================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        mBinding = DataBindingUtil.bind(view);

        component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(CatalogFragment.Component.class, component);
        }
        component.inject(this);

        mCatalogPresenter.takeView(this);
        mCatalogPresenter.initView();

        setupAuthDialog();
        setupCountProductInBucket();

        mBinding.addToCardButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mCatalogPresenter.dropView();
        mCatalogPresenter = null;
        component = null;
        DaggerService.unregisterScope(CatalogScope.class);
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (R.id.add_to_card_button == v.getId()) {
            mCatalogPresenter.clickOnBuyButton(mBinding.productViewPager.getCurrentItem());
        }
    }

    //endregion

    //region ================= ICatalogView =================

    @Override
    public void showAddToCardMessage(ProductDto productDto) {
    }

    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter catalogAdapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productList) {
            catalogAdapter.addItem(product);
        }
        mBinding.productViewPager.setAdapter(catalogAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.productViewPager, true);
    }

    @Override
    public void showAuthScreen() {
        mAuthDialog.show();
    }

    @Override
    public void hideAuthDialog() {
        if (mAuthDialog != null) {
            mAuthDialog.dismiss();
        }
    }

    @Override
    public String getAuthEmail() {
        return mBindingAuthDialog.loginEmailEditText.getText().toString();
    }

    @Override
    public String getAuthPassword() {
        return mBindingAuthDialog.loginPasswordEditText.getText().toString();
    }

    //endregion

    //region ================= setup UI =================

    private void setupAuthDialog() {
        boolean wrapInScrollView = true;
        mAuthDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.title_auth_dialog)
                .customView(R.layout.card_view_auth_panel, wrapInScrollView)
                .positiveText(R.string.positive_auth)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mCatalogPresenter.saveAuthUser();
                    }
                })
                .build();
        View view = mAuthDialog.getCustomView();
        mBindingAuthDialog = DataBindingUtil.bind(view);
    }

    private void setupCountProductInBucket() {
        mCatalogPresenter.updateCountProductInBucket();
    }

    //endregion

    //region ================= DI =================

    private Component createDaggerComponent() {
        return DaggerCatalogFragment_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogFragment fragment);
    }

    //endregion
}
