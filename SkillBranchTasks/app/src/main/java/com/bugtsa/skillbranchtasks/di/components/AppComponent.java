package com.bugtsa.skillbranchtasks.di.components;

import android.content.Context;

import com.bugtsa.skillbranchtasks.di.modules.AppModule;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();
}
