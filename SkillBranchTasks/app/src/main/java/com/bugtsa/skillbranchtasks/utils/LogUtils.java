package com.bugtsa.skillbranchtasks.utils;

import android.util.Log;

public class LogUtils {
    public static final String TAG = "Skill-Branch-Tasks";
    private static boolean isDebug = true;

    public static void d(String message) {
        if (isDebug) {
            Log.d(TAG, message);
        }
    }

    public static void d(String tag, String message) {
        if (isDebug) {
            Log.d(tag, message);
        }
    }
}
