package com.bugtsa.skillbranchtasks.utils;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtils {

    private static final String PASSWORD_PATTERN =
            "(?=.*[0-9])(?=.*[a-zA-Z]).{7,}";

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidPassword(CharSequence targetPassword) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(targetPassword);
        return matcher.matches();
    }
}
