package com.bugtsa.skillbranchtasks.mvp.presenters;

import android.net.Uri;

import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.scopes.RootScope;
import com.bugtsa.skillbranchtasks.mvp.models.RootModel;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.IRootPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IRootView;

import javax.inject.Inject;

import dagger.Provides;

public class RootPresenter extends AbstractPresenter<IRootView> implements IRootPresenter {

    @Inject
    RootModel mRootModel;

    public RootPresenter () {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
//            component = DaggerService.createComponent(Component.class, Module.class);
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }

    //region ================= AbstractPresenter =================

    @Override
    public void initView() {

    }

    //endregion

    //region ================= IRootPresenter =================

    @Override
    public Uri getUriDrawerAvatar() {
        return mRootModel.getSelectedImage();
    }

    @Override
    public void saveCountProductInBucket(int countProductInBucket) {
        mRootModel.saveCountProductInBucket(countProductInBucket);
    }

    @Override
    public int getCountProductInBucket() {
        return mRootModel.getCountProductInBucket();
    }

    //endregion

    //region ================= DI =================

    private Component createDaggerComponent() {
        return DaggerRootPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootModel provideRootModel() {
            return new RootModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @RootScope
    interface Component {

        void inject(RootPresenter presenter);
    }


    //endregion
}
