package com.bugtsa.skillbranchtasks.mvp.models;

public interface IAuthModel {

    void clearUserData();

    void saveUserData(String email, String password);

    void getDataFromNetwork();
}
