package com.bugtsa.skillbranchtasks.data.storage.etems;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

public class Goods {

    private static String name = "Мёд липовый";

    private static String desc = "\"А. Д'Арбо\", Австрия.\n" +
            "Мед D'arbo натуральный липовый 500г стекло.\n" +
            "Мед, собранный с цветков липы, обладает нежной кремовой консистенцией и\n" +
            "тонким ароматом. Его богатый вкус имеет множество оттенков: от мягкого\n" +
            "цветочного к сладкому, оставляет приятное послевкусие.\n" +
            "Низкотемпературная обработка меда позволяет сохранить все полезные\n" +
            "вещества и неповторимый вкус. Не рекомендуется нагревать свыше 40'C.\n" +
            "Пищевая ценность в 100г продукта: жиры 0г, белки 0,4г, углеводы 75г.\n" +
            "Энергетическая ценность: 302 Ккал.\n" +
            "Хранить при комнатной температуре в темном месте.\n" +
            "Срок хранения: 24 месяца.";
    private static String desc_short = "\"А. Д'Арбо\", Австрия.\n" +
            "Мед D'arbo натуральный липовый 500г стекло.\n" +
            "Мед, собранный с цветков липы, обладает нежной кремовой консистенцией и\n" +
            "тонким ароматом. Его богатый вкус имеет множество оттенков: от мягкого\n" +
            "цветочного к сладкому, оставляет приятное послевкусие.\n" +
            "Срок хранения: 24 месяца.";

    private static String url = "http://www.utkonos.ru/item/1977/1363637?_openstat=bWFya2V0LnlhbmRleC5ydTvQnNC10LQgRCdhcmJvINC90LDRgtGD0YDQsNC70YzQvdGL0Lkg0LvQuNC_0L7QstGL0LkgNTAw0LMg0YHRgtC10LrQu9C-OzlOaDVDdG1MclZNLURocXRFMWpWT2c7&utm_campaign=cube_market&utm_medium=cpc&utm_source=ymarket&utm_term=3193449&ymclid=778092039416178019000005";
    private static String url2 = "http://www.arabio.ru/uploads/image/celmed.jpg";

    private static int price = 614;

    public static ProductDto mProductDto = new ProductDto(1, name, url2, desc_short, price, 1);
}
