package com.bugtsa.skillbranchtasks;

import android.app.Application;

import com.bugtsa.skillbranchtasks.di.components.AppComponent;
import com.bugtsa.skillbranchtasks.di.components.DaggerAppComponent;
import com.bugtsa.skillbranchtasks.di.modules.AppModule;

public class SkillBranchTasksApplication extends Application {

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    private static AppComponent sAppComponent;

    /**
     * Инициализирует AppComponent
     */
    @Override
    public void onCreate() {
        super.onCreate();
        createComponent();
    }

    private void createComponent() {
        sAppComponent = DaggerAppComponent.builder()
        .appModule(new AppModule(getApplicationContext()))
        .build();
    }
}
