package com.bugtsa.skillbranchtasks.mvp.models;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

import java.util.List;

public class CatalogModel extends AbstractModel{

    public CatalogModel() {
    }

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    public int getCountProductInBucket() {
        return mDataManager.getPreferencesManager().getBasketCounter();
    }
}
