package com.bugtsa.skillbranchtasks.data.managers;

import com.bugtsa.skillbranchtasks.SkillBranchTasksApplication;
import com.bugtsa.skillbranchtasks.data.network.RestService;
import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.components.DaggerDataManagerComponent;
import com.bugtsa.skillbranchtasks.di.components.DataManagerComponent;
import com.bugtsa.skillbranchtasks.di.modules.LocalModule;
import com.bugtsa.skillbranchtasks.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;

    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);

        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(SkillBranchTasksApplication.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

        generateMockData();
    }

    /**
     * Получает SharedPreferences
     *
     * @return SharedPreferences
     */
    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public ProductDto getProductById(int productId) {
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto productDto) {

    }

    public List<ProductDto> getProductList() {
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        String mockDescription = "description 1 description 1 description 1 description 1 description 1";
        mMockProductList.add(new ProductDto(0, "test 0", "http://www.arabio.ru/uploads/image/celmed.jpg", mockDescription, 3200, 1));
        mMockProductList.add(new ProductDto(1, "test 1", "imageUrl", mockDescription, 2200, 1));
        mMockProductList.add(new ProductDto(2, "test 2", "imageUrl", mockDescription, 200, 1));
        mMockProductList.add(new ProductDto(3, "test 3", "imageUrl", mockDescription, 300, 1));
        mMockProductList.add(new ProductDto(4, "test 4", "imageUrl", mockDescription, 400, 1));
        mMockProductList.add(new ProductDto(5, "test 5", "imageUrl", mockDescription, 500, 1));
        mMockProductList.add(new ProductDto(6, "test 6", "imageUrl", mockDescription, 600, 1));
        mMockProductList.add(new ProductDto(7, "test", "imageUrl", mockDescription, 700, 1));
        mMockProductList.add(new ProductDto(8, "test", "imageUrl", mockDescription, 800, 1));
        mMockProductList.add(new ProductDto(9, "test", "imageUrl", mockDescription, 900, 1));
        mMockProductList.add(new ProductDto(10, "test", "imageUrl", mockDescription, 1000, 1));
        mMockProductList.add(new ProductDto(11, "test", "imageUrl", mockDescription, 1100, 1));
        mMockProductList.add(new ProductDto(12, "test", "imageUrl", mockDescription, 1200, 1));
        mMockProductList.add(new ProductDto(13, "test", "imageUrl", mockDescription, 1000, 1));
        mMockProductList.add(new ProductDto(14, "test", "imageUrl", mockDescription, 1100, 1));
        mMockProductList.add(new ProductDto(15, "test", "imageUrl", mockDescription, 1200, 1));
        mMockProductList.add(new ProductDto(16, "test", "imageUrl", mockDescription, 1200, 1));
        mMockProductList.add(new ProductDto(17, "test", "imageUrl", mockDescription, 1000, 1));
        mMockProductList.add(new ProductDto(18, "test", "imageUrl", mockDescription, 1100, 1));
        mMockProductList.add(new ProductDto(19, "test", "imageUrl", mockDescription, 1200, 1));
    }

    public boolean isUserAuth() {
        return false;
    }
}
