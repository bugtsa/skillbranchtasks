package com.bugtsa.skillbranchtasks.mvp.views;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

import java.util.List;

public interface ICatalogView extends IView {

    void showAddToCardMessage(ProductDto productDto);

    void showCatalogView(List<ProductDto> productList);

    void showAuthScreen();

    void hideAuthDialog();

    String getAuthEmail();
    String getAuthPassword();
}
