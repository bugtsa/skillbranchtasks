package com.bugtsa.skillbranchtasks.mvp.presenters.interfaces;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

public interface IProductPresenter {

    ProductDto addProduct();
    ProductDto deleteProduct();
}
