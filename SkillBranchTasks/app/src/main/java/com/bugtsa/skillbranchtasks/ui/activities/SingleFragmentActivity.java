package com.bugtsa.skillbranchtasks.ui.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.utils.ConstantManager;
import com.bugtsa.skillbranchtasks.utils.FragmentUtils;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        Fragment fragment = FragmentUtils.getFragmentById(this, R.id.fragment_auth_container);
        if (fragment == null) {
            fragment = createFragment();
            FragmentUtils.addFragment(this,
                    R.id.fragment_auth_container,
                    fragment,
                    ConstantManager.TAG_AUTH_FRAGMENT,
                    false);
        }
    }
}
