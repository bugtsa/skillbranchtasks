package com.bugtsa.skillbranchtasks.mvp.presenters;


import com.bugtsa.skillbranchtasks.mvp.views.IView;

public abstract class AbstractPresenter<T extends IView> {

    private T mView;

    public void takeView(T view) {
        mView = view;
    }

    public void dropView() {
        mView = null;
    }

    public abstract void initView();

    public T getView() {
        return mView;
    }
}
