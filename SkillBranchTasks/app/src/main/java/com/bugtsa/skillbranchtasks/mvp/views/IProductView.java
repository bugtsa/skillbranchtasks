package com.bugtsa.skillbranchtasks.mvp.views;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

public interface IProductView extends IView{

    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
