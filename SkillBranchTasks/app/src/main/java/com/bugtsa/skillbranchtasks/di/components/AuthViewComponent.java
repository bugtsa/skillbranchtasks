package com.bugtsa.skillbranchtasks.di.components;

import com.bugtsa.skillbranchtasks.di.modules.AuthViewModule;
import com.bugtsa.skillbranchtasks.di.scopes.AuthScope;
import com.bugtsa.skillbranchtasks.ui.activities.AuthActivity;
import com.bugtsa.skillbranchtasks.ui.custom_views.AuthPanel;
import com.bugtsa.skillbranchtasks.ui.fragments.AuthFragment;

@dagger.Component(modules = AuthViewModule.class)
@AuthScope
public interface AuthViewComponent {
    void inject(AuthActivity activity);

    void inject(AuthFragment fragment);

    void inject(AuthPanel authPanel);
}

