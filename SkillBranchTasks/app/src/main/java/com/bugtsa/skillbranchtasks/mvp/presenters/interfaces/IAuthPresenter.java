package com.bugtsa.skillbranchtasks.mvp.presenters.interfaces;

import android.support.annotation.Nullable;

import com.bugtsa.skillbranchtasks.mvp.views.IAuthView;

public interface IAuthPresenter {

    void onResume();

    void onPause();

    @Nullable
    IAuthView getView();

    void clickOnLogin();

    void clickOnShowCatalog();

    void clickOnVk();

    void clickOnTwitter();

    void clickOnFb();

    boolean isUserAuth();

    boolean isCorrectInput();

    void editTextChanged(int editTextKey);


}
