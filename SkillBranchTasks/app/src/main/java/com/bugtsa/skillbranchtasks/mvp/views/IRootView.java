package com.bugtsa.skillbranchtasks.mvp.views;


public interface IRootView extends IBaseView {

    void showPutProductInBucketMessage(String productName);

    void increaseProductCounter(int increaseProduct);

    void updateProductCounter(int countProductInBucket);
}
