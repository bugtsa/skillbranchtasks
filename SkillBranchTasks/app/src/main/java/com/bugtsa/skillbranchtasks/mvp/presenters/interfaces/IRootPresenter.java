package com.bugtsa.skillbranchtasks.mvp.presenters.interfaces;


import android.net.Uri;

public interface IRootPresenter {

    Uri getUriDrawerAvatar();

    void saveCountProductInBucket(int countProductInBucket);

    int getCountProductInBucket();
}
