package com.bugtsa.skillbranchtasks.ui.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bugtsa.skillbranchtasks.R;
import com.bugtsa.skillbranchtasks.SkillBranchTasksApplication;
import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;
import com.bugtsa.skillbranchtasks.databinding.FragmentProductBinding;
import com.bugtsa.skillbranchtasks.di.DaggerService;
import com.bugtsa.skillbranchtasks.di.components.DaggerPicassoComponent;
import com.bugtsa.skillbranchtasks.di.components.PicassoComponent;
import com.bugtsa.skillbranchtasks.di.modules.PicassoCacheModule;
import com.bugtsa.skillbranchtasks.di.scopes.ProductScope;
import com.bugtsa.skillbranchtasks.mvp.presenters.ProductPresenter;
import com.bugtsa.skillbranchtasks.mvp.views.IProductView;
import com.bugtsa.skillbranchtasks.utils.LogUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;

import static com.bugtsa.skillbranchtasks.utils.ConstantManager.MINIMUM_PRODUCT_QUANTITY;
import static com.bugtsa.skillbranchtasks.utils.ConstantManager.PRODUCT_PARCELABLE_KEY;

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {

    private static final String TAG = ProductFragment.class.getSimpleName();

    FragmentProductBinding mBinding;

    @Inject
    Picasso mPicasso;

    @Inject
    ProductPresenter mProductPresenter;

    Component component;

//    private static ProductFragment sProductFragment;

    private ProductDto mProduct;

    public ProductFragment() {
    }

    public static ProductFragment newInstance(ProductDto product) {

        Bundle args = new Bundle();
        args.putParcelable(PRODUCT_PARCELABLE_KEY, product);

//        if (sProductFragment == null) {
//            sProductFragment= new ProductFragment();
//
//        }
        ProductFragment sProductFragment = new ProductFragment();
        sProductFragment.setArguments(args);
        return sProductFragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mProduct = bundle.getParcelable(PRODUCT_PARCELABLE_KEY);

            component = DaggerService.getComponent(Component.class);
            if (component == null) {
                component = createDaggerComponent(mProduct);
                DaggerService.registerComponent(Component.class, component);
            }
            component.inject(this);

            mProductPresenter.setProduct(mProduct);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        mBinding = DataBindingUtil.bind(view);
        readBundle(getArguments());
        mProductPresenter.takeView(this);
        mProductPresenter.initView();
        mBinding.minusButton.setOnClickListener(this);
        mBinding.plusButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mProductPresenter.dropView();
        mProductPresenter = null;
        component = null;
        DaggerService.unregisterScope(ProductScope.class);
        super.onDestroyView();
    }

    //region ================= IProductView =================

    @Override
    public void showProductView(final ProductDto product) {
        mBinding.setProduct(product);
        mPicasso.load(product.getImageUrl())
                .error(R.drawable.radio_image)
                .placeholder(R.drawable.radio_image)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mBinding.productImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        LogUtils.d(TAG, getString(R.string.load_from_disk_cache));
                    }

                    @Override
                    public void onError() {
                        LogUtils.d(TAG, getString(R.string.not_load_from_cache));
                        mPicasso.load(product.getImageUrl())
                                .error(R.drawable.radio_image)
                                .placeholder(R.drawable.radio_image)
                                .fit()
                                .centerCrop()
                                .into(mBinding.productImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        LogUtils.d(TAG, getString(R.string.done_show_image));
                                    }

                                    @Override
                                    public void onError() {
                                        LogUtils.d(TAG, getString(R.string.could_not_fetch_image));
                                    }
                                });
                    }
                });
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        if (product.getCount() >= MINIMUM_PRODUCT_QUANTITY) {
            mBinding.productCountTextView.setText(String.valueOf(product.getCount()));
            mBinding.productPriceTextView.setText(String.valueOf(product.getCount() * product.getPrice()));
        }
    }

    //endregion

    @Override
    public void onClick(View v) {
        mProductPresenter.setProduct(mProduct);
        switch (v.getId()) {
            case R.id.minus_button:
                updateProductCountView(mProductPresenter.deleteProduct());
                break;
            case R.id.plus_button:
                updateProductCountView(mProductPresenter.addProduct());
                break;
        }
    }

    //region ============ DI ============

    @dagger.Module
    public class Module {

        ProductDto mProductDto;

        public Module(ProductDto productDto) {
            mProductDto = productDto;
        }

        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return ProductPresenter.newInstance();
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductFragment fragment);
    }

    private Component createDaggerComponent(ProductDto product) {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(SkillBranchTasksApplication.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }
        return DaggerProductFragment_Component.builder()
                .picassoComponent(picassoComponent)
                .module(new Module(product))
                .build();
    }

    //endregion
}
