package com.bugtsa.skillbranchtasks.di.components;

import com.bugtsa.skillbranchtasks.data.managers.DataManager;
import com.bugtsa.skillbranchtasks.di.modules.LocalModule;
import com.bugtsa.skillbranchtasks.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {

    void inject(DataManager dataManager);
}
