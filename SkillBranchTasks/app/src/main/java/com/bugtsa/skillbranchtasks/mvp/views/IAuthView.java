package com.bugtsa.skillbranchtasks.mvp.views;

import com.android.annotations.Nullable;
import com.bugtsa.skillbranchtasks.mvp.presenters.interfaces.IAuthPresenter;
import com.bugtsa.skillbranchtasks.ui.custom_views.AuthPanel;

public interface IAuthView extends IBaseView {

    void showValidEmailEditText();
    void showErrorEmailEditText();

    void showValidPasswordEditText();
    void showErrorPasswordEditText();

    IAuthPresenter getAuthPresenter();

    void setTextLoginButton(boolean isUserAuth);

    void enabledLoginButton();
    void disabledLoginButton();

    @Nullable
    AuthPanel getAuthPanel();

    void showCatalogScreen();
}
