package com.bugtsa.skillbranchtasks.data.events;

public class StartLoadInViewEvent {

    private long mTimeLoop;

    public StartLoadInViewEvent(long timeLoop) {
        mTimeLoop = timeLoop;
    }

    public long getTimeLoop() {
        return mTimeLoop;
    }
}
