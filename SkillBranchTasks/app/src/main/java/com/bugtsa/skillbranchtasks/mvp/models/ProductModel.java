package com.bugtsa.skillbranchtasks.mvp.models;


import com.bugtsa.skillbranchtasks.data.storage.dto.ProductDto;

public class ProductModel extends AbstractModel {

    public ProductDto getProductById(int productId) {
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto productDto) {
        mDataManager.updateProduct(productDto);
    }
}
