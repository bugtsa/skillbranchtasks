package com.bugtsa.skillbranchtasks.data.storage.dto;


import android.os.Parcel;
import android.os.Parcelable;

public class ProductDto implements Parcelable {

    private int id;

    private String productName;

    private String imageUrl;
    private String description;
    private double price;
    private int count;

    public ProductDto(int id, String productName, String imageUrl, String description, double price, int count) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    //region ================= Parcelable =================

    protected ProductDto(Parcel in) {
        id = in.readInt();
        productName = in.readString();
        imageUrl = in.readString();
        description = in.readString();
        price = in.readInt();
        count = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(productName);
        dest.writeString(imageUrl);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeInt(count);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductDto> CREATOR = new Creator<ProductDto>() {
        @Override
        public ProductDto createFromParcel(Parcel in) {
            return new ProductDto(in);
        }

        @Override
        public ProductDto[] newArray(int size) {
            return new ProductDto[size];
        }
    };
    //endregion

    //region ================= Getters =================

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public double getPrice() {
        return price;
    }

//    public double getPrice() { return Integer.toString(price) +".-";}

    public int getCount() {
        return count;
    }

    public String getDescription() {
        return description;
    }

    //endregion


    public void deleteProduct() {
        count--;
    }

    public void addProduct() {
        count++;
    }
}
