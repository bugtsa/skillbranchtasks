package com.bugtsa.skillbranchtasks.ui.activities;


import android.support.v4.app.Fragment;

import com.bugtsa.skillbranchtasks.ui.fragments.AuthFragment;

public class AuthActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return AuthFragment.newInstance();
    }
}
