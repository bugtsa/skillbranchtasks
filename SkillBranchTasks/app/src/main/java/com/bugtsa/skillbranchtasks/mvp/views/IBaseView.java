package com.bugtsa.skillbranchtasks.mvp.views;

public interface IBaseView extends IView {

    void showMessage(String message);

    void showErrors(Throwable e);

    void showLoad();

    void hideLoad();
}
